package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * 
 */
public class AnotherConcurrentGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.07;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    /**
     * 
     */
    //TODO refactor from current gui
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        display.setText(Integer.toString(0));
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        /*
         * Create the counter agent and start it. This is actually not so good:
         * thread management should be left to
         * java.util.concurrent.ExecutorService
         */
        final TimeAgent timeAgent = new TimeAgent();
        new Thread(timeAgent).start();
        final Agent agent = new Agent();
        new Thread(agent).start();
        /*
         * Register a listener that stops it
         */
        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // agent dopra deve essere final
                agent.stopCounting();
            }
        });
        down.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.downCounting(true);
            }
        });
        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.downCounting(false);
            }
        });
   }
    /*
     * The counter agent is implemented as a nested class. This makes it
     * invisible outside and incapsulated.
     */
    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private volatile boolean down;
        private int counter;

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if (this.down) {
                        this.counter--;
                        AnotherConcurrentGUI.this.down.setEnabled(false);
                        AnotherConcurrentGUI.this.up.setEnabled(true);
                    } else {
                        this.counter++;
                        AnotherConcurrentGUI.this.up.setEnabled(false);
                        AnotherConcurrentGUI.this.down.setEnabled(true);
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        /**
         * Excternal command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
            AnotherConcurrentGUI.this.down.setEnabled(false);
            AnotherConcurrentGUI.this.up.setEnabled(false);
            AnotherConcurrentGUI.this.stop.setEnabled(false);
        }
        /**
         * 
         * @param down
         */
        public void downCounting(final boolean down) {
            this.down = down;
        }
    }
    private class TimeAgent implements Runnable {
        private static final int TIME = 10 * 1000;
        private final long startTime = System.currentTimeMillis();
        private long stopTime; 

        public void run() {
            while (this.stopTime - this.startTime < TimeAgent.TIME) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            stopTime = System.currentTimeMillis();
                        }
                    });
                } catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            this.stop();
        }
        public void stop() {
            AnotherConcurrentGUI.this.down.setEnabled(false);
            AnotherConcurrentGUI.this.up.setEnabled(false);
            AnotherConcurrentGUI.this.stop.setEnabled(false);
            //TODO blocccare thread agent
        }
    }
}
