package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Implements SumMatrix interface.
 */
public class MultiThreadedSumMatrix implements SumMatrix {
    private final int nthread;

    /**
     * 
     * @param nthread
     *            no. of thread performing the sum.
     */
    public MultiThreadedSumMatrix(final int nthread) {
        this.nthread = nthread;
    }
    /**
     * 
     */
    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startrow;
        private final int nrows;
        private double res;
        /**
         * Build a new worker.
         * 
         * @param matrix
         *            the matrix to sum
         * @param startrow
         *            the initial position for this worker
         * @param nrows
         *            the no. of rows to sum up for this worker
         */
        Worker(final double[][] matrix, final int startrow, final int nrows) {
            super();
            this.matrix = matrix;
            this.startrow = startrow;
            this.nrows = nrows;
        }

        @Override
        public void run() {
            System.out.println("Working from row " + startrow + " to position " + (startrow + nrows - 1));
            for (int i = startrow; i < matrix.length && i < startrow + nrows; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    this.res += this.matrix[i][j];
                }
            }
        }

        /**
         * Returns the result of summing up the integers within the list.
         * 
         * @return the sum of every element in the matrix
         */
        public double getResult() {
            return this.res;
        }
    }
    @Override
    public double sum(final double[][] matrix) {
        /*
         * matrix.lenght return number of rows
         */
        final int size = matrix.length % nthread + matrix.length / nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = IntStream.iterate(0, start -> start + size)
                .limit(nthread)
                .mapToObj(start -> new Worker(matrix, start, size))
                .collect(Collectors.toList());
        /*
         * Start them
         */
        workers.forEach(Thread::start);
        /*
         * Wait for every one of them to finish
         */
        workers.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        /*
         * Return the sum
         */
        return workers.stream().mapToDouble(Worker::getResult).sum();
    }

}
